//Load express module with `require` directive
"use strict";
process.env.NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV : "local";
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`
});
var express = require('express');
const logger = require("morgan");

var app = express();


app.use(logger("dev"));
app.use("/", express.static(__dirname + "/public"));

//Define request response in root URL (/)
app.get('/hello', function (req, res) {
  res.send('Hello World');
});

//Launch listening server on port 8080
app.listen(process.env.PORT, function () {
  console.log(`App listening on port ${process.env.PORT}!`);
});