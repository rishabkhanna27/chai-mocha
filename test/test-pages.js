var expect = require('chai').expect;
var request = require('request');

describe('Status and content', function () {
    describe('Main page', function () {
        it('status', function (done) {
            request('http://localhost:8080/hello', function (error, response) {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it('content', function (done) {
            request('http://localhost:8080/hello', function (error, response, body) {
                expect(body).to.equal('Hello World');
                done();
            });
        });
    });
    describe('About page', function () {
        it('status', function (done) {
            request('http://localhost:8080/about', function (error, response) {
                expect(response.statusCode).to.equal(404);
                done();
            });
        });
    });
});